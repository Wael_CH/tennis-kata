package com.total.digitalfactory.tenniskata.domain.game;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameTest {

    private Game game = new Game();

    @Test
    public void
    GIVEN_player_one_has_0_points_AND_player_two_has_0_points_WHEN_game_score_THEN_should_return_love_all() {
        // GIVEN
        Player player1 = new Player("Player ONE");
        Player player2 = new Player("Player TWO");

        // WHEN
        String result = this.game.getScore(player1, player2);

        // THEN
        assertEquals("Love All", result);
    }

    @Test
    public void
    GIVEN_player_one_has_2_points_AND_player_two_has_0_points_WHEN_game_score_THEN_should_return_fifteen_love() {
        // GIVEN
        Player player1 = new Player("Player ONE");
        player1.scorePoint();
        player1.scorePoint();

        Player player2 = new Player("Player TWO");

        // WHEN
        String result = this.game.getScore(player1, player2);

        // THEN
        assertEquals("Thirty - Love", result);
    }

    @Test
    public void
    GIVEN_player_one_has_3_points_AND_player_two_has_3_points_WHEN_game_score_THEN_should_return_deuce() {
        // GIVEN
        Player player1 = new Player("Player ONE");
        player1.scorePoint();
        player1.scorePoint();
        player1.scorePoint();

        Player player2 = new Player("Player TWO");
        player2.scorePoint();
        player2.scorePoint();
        player2.scorePoint();

        // WHEN
        String result = this.game.getScore(player1, player2);

        // THEN
        assertEquals("Deuce", result);
    }

    @Test
    public void
    GIVEN_player_one_has_4_points_AND_player_two_has_3_points_WHEN_game_score_THEN_should_return_advantage_player_one() {
        // GIVEN
        Player player1 = new Player("Player ONE");
        player1.scorePoint();
        player1.scorePoint();
        player1.scorePoint();
        player1.scorePoint();

        Player player2 = new Player("Player TWO");
        player2.scorePoint();
        player2.scorePoint();
        player2.scorePoint();

        // WHEN
        String result = this.game.getScore(player1, player2);

        // THEN
        assertEquals("Advantage Player ONE", result);
    }

    @Test
    public void
    GIVEN_player_one_has_4_points_AND_player_two_has_5_points_WHEN_game_score_THEN_should_return_advantage_player_two() {
        // GIVEN
        Player player1 = new Player("Player ONE");
        player1.scorePoint();
        player1.scorePoint();
        player1.scorePoint();
        player1.scorePoint();

        Player player2 = new Player("Player TWO");
        player2.scorePoint();
        player2.scorePoint();
        player2.scorePoint();
        player2.scorePoint();
        player2.scorePoint();

        // WHEN
        String result = this.game.getScore(player1, player2);

        // THEN
        assertEquals("Advantage Player TWO", result);
    }

    @Test
    public void
    GIVEN_player_one_has_4_points_AND_player_two_has_2_points_WHEN_game_score_THEN_should_return_win_for_player_one() {
        // GIVEN
        Player player1 = new Player("Player ONE");
        player1.scorePoint();
        player1.scorePoint();
        player1.scorePoint();
        player1.scorePoint();

        Player player2 = new Player("Player TWO");
        player2.scorePoint();
        player2.scorePoint();


        // WHEN
        String result = this.game.getScore(player1, player2);

        // THEN
        assertEquals("Win for Player ONE", result);
    }

    
  

}
