package com.total.digitalfactory.tenniskata.domain.game;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlayerTest {

    private Game game = new Game();

    @Test
    public void
    GIVEN_player_WHEN_score_1_point_THEN_should_return_1() {
        // GIVEN
        Player player = new Player("Player");

        // WHEN
        player.scorePoint();

        // THEN
        assertEquals(1, player.getPoints());
    }

    @Test
    public void
    GIVEN_player_has_2_points_WHEN_check_less_than_3_points_THEN_should_return_true() {
        // GIVEN
        Player player = new Player("Player");
        player.scorePoint();
        player.scorePoint();

        // WHEN
        boolean result = player.hasLessThanThreePoints();

        // THEN
        assertEquals(true, result);
    }

    @Test
    public void
    GIVEN_player_has_4_points_WHEN_check_more_than_3_points_THEN_should_return_true() {
        // GIVEN
        Player player = new Player("Player");
        player.scorePoint();
        player.scorePoint();
        player.scorePoint();
        player.scorePoint();

        // WHEN
        boolean result = player.hasMoreThanThreePoints();

        // THEN
        assertEquals(true, result);
    }

    @Test
    public void
    GIVEN_player_one_has_2_points_AND_player_two_has_2_points_WHEN_compare_points_THEN_should_return_0() {
        // GIVEN
        Player player1 = new Player("Player ONE");
        player1.scorePoint();
        player1.scorePoint();

        Player player2 = new Player("Player TWO");
        player2.scorePoint();
        player2.scorePoint();

        // WHEN
        int result = player1.compareTo(player2);

        // THEN
        assertEquals(0, result);
    }





}
