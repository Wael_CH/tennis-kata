package com.total.digitalfactory.tenniskata.domain.game;

public class Player implements  Comparable<Player> {

    private String name;
    private Integer points;

    public Player(String name) {
        this.name = name;
        this.points = 0;
    }

    @Override
    public int compareTo(Player other) {
        return this.points.compareTo(other.getPoints());
    }

    public Integer getPointsDifference(Player other) {
        return Math.abs(this.points - other.getPoints());
    }

    public void scorePoint(){
        points++;
    }

    public String getName() {
        return name;
    }

    public Integer getPoints() {
        return points;
    }

    public Boolean hasMoreThanThreePoints() {
        return this.points > 3;
    }

    public Boolean hasLessThanThreePoints() {
        return this.points < 3;
    }


}
