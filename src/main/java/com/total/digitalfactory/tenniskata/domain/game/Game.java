package com.total.digitalfactory.tenniskata.domain.game;

public class Game {

    public String getScore(Player playerOne, Player playerTwo) {
        if (playersHaveSamePointsNumber(playerOne, playerTwo)) {
            return equality(playerOne);
        }
        if (atLeastLOnePlayerHasMoreThanThreePoints(playerOne, playerTwo)) {
            return advantageOrWin(playerOne, playerTwo);
        }
        return otherCases(playerOne.getPoints(), playerTwo.getPoints());
    }

    private boolean atLeastLOnePlayerHasMoreThanThreePoints(Player playerOne, Player playerTwo) {
        return playerOne.hasMoreThanThreePoints() || playerTwo.hasMoreThanThreePoints();
    }

    private boolean playersHaveSamePointsNumber(Player playerOne, Player playerTwo) {
        return playerOne.compareTo(playerTwo) == 0;
    }

    private String equality(Player player) {
        if (player.hasLessThanThreePoints()) {
            return Constants.SCORE_LABELS[player.getPoints()] + Constants.ALL;
        }
        return Constants.DEUCE;
    }

    private String advantageOrWin(Player playerOne, Player playerTwo) {
        if (playerOne.getPointsDifference(playerTwo) >= 2) {
            return Constants.WIN + getPlayerWithHighestScore(playerOne, playerTwo);
        }
        return Constants.ADVANTAGE + getPlayerWithHighestScore(playerOne, playerTwo);
    }

    private String getPlayerWithHighestScore(Player playerOne, Player playerTwo) {
        if (playerOne.compareTo(playerTwo) >= 0) {
            return playerOne.getName();
        }
        return playerTwo.getName();
    }

    private String otherCases(int playerOnePoints, int playerTwoPoints) {
        return Constants.SCORE_LABELS[playerOnePoints] + " - " + Constants.SCORE_LABELS[playerTwoPoints];
    }

}
