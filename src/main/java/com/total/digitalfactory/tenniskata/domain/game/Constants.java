package com.total.digitalfactory.tenniskata.domain.game;

public class Constants {

    private Constants() {
    }

    static final String[] SCORE_LABELS = new String[] {"Love", "Fifteen", "Thirty", "Forty"};
    static final String WIN = "Win for ";
    static final String ADVANTAGE = "Advantage ";
    static final String DEUCE = "Deuce";
    static final String ALL = " All";

}
